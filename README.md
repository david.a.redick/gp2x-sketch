# What is gp2x-sketch?

This is a simple drawing program, kind of like an etch-a-sketch.

This program hasn't been updated since 2006-12-16.

To my knowledge there are no bugs or GP2X API changes.

# How to Use

Copy the Sketch directory to your SD memory card.
Then run as a game from your GP2X.

Use the joy stick to move the cursor.

Press A to draw Red
Press B to draw Blue.
Press X to draw White.
Press Y to draw Green.
Press L to draw Yellow.
Press R to draw Purple.
Press Select to wipe the screen (fill with black).
Press Start to exit.

# How to compile - Windows XP

Download the Unoffical GP2X Dev. Kit from: http://archive.gp2x.de/cgi-bin/cfiles.cgi?0,0,0,0,14,1362

Update the path variable in Start > Control > System > Advance > Enviroment Variables

Add the path: `C:\devkitGP2X\bin;C:\devkitGP2X\minsys\bin`

Open a shell (aka `cmd`):

	$ cd c:\projects\gp2x-sketch\Source
	$ make

NOTE: If you have a different Dev. Kit then you'll may have to edit the Makefile.

# How to compile - *nix

On your own.

If you find a dev. kit that works then please email me.

# Tested On

Tested on GP2X firmware version 2.

# Project

All code and graphics are in the public domain.

Project Home: https://github.com/david-redick/gp2x-sketch

Maintainer: David A. Redick tinyweldingtorch@gmail.com
