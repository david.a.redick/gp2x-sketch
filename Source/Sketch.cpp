#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "SDL.h"

////////////////////////////////////////////////////////////////////////////////
// Functions

bool StartUp();		// return false if error
void Run();
void ShutDown();
void UpdateScreen();

////////////////////////////////////////////////////////////////////////////////
// Globals

enum GP2X_INPUT
{
	GP2X_JOY_UP,
	GP2X_JOY_UP_LEFT,
	GP2X_JOY_LEFT,
	GP2X_JOY_DOWN_LEFT,
	GP2X_JOY_DOWN,
	GP2X_JOY_DOWN_RIGHT,
	GP2X_JOY_RIGHT,
	GP2X_JOY_UP_RIGHT,
	GP2X_START,
	GP2X_SELECT,
	GP2X_R,
	GP2X_L,
	GP2X_A,
	GP2X_B,
	GP2X_Y,
	GP2X_X,
	GP2X_VOL_UP,
	GP2X_VOL_DOWN,
	GP2X_JOY_PUSH,
	GP2X_INPUT_MAX
};

const Uint16 MIN_X =   0;
const Uint16 MIN_Y =   0;
const Uint16 MAX_X = 320;
const Uint16 MAX_Y = 240;

FILE*		g_pLogFile;
SDL_Surface*	g_pScreen;
SDL_Joystick*	g_pJoystick;

bool g_input[GP2X_INPUT_MAX] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false
};

bool			g_bNeedUpdate	= true;

Uint16		g_iX			= MIN_X;
Uint16		g_iY			= MIN_Y;

////////////////////////////////////////////////////////////////////////////////
// 16 bit color format
//
// +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
// | F | E | D | C | B | A | 9 | 8 | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
// +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
// | R | R | R | R | R | G | G | G | G | G | G | B | B | B | B | B |
// +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
//
// RRRR RGGG GGGB BBBB
// 1111 1000 0000 0000	RED
// 0000 0111 1110 0000	GREEN
// 0000 0000 0001 1111	BLUE
// 0000 0111 1111 1111	CYAN		(GREEN + BLUE)
// 1111 1000 0001 1111	PINK		(RED + BLUE)
// 1111 1111 1110 0000	YELLOW	(RED + GREEN)
enum COLORS
{
	BLACK	= 0x0000,
	WHITE	= 0xffff,
	RED		= 0xf100,
	GREEN	= 0x07e0,
	BLUE		= 0x001f,
	CYAN		= 0x07ff,
	PINK		= 0xf81f,
	YELLOW	= 0xffe0
};

////////////////////////////////////////////////////////////////////////////////
  
int main(int argc, char *argv[])
{
	if(StartUp())
		Run();

	ShutDown();

	return EXIT_SUCCESS;
}

////////////////////////////////////////////////////////////////////////////////
// return false if error

bool StartUp()
{
	g_pLogFile = fopen("log.txt", "w");
	if(g_pLogFile == NULL)
	{
		return false;
	}

	fprintf(g_pLogFile, "StartUp()\n");

	if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK) < 0)
	{
		fprintf(g_pLogFile, "ERROR - SDL_Init(): %s\n", SDL_GetError());
		return false;
	}

	g_pScreen = SDL_SetVideoMode(MAX_X, MAX_Y, 16, SDL_SWSURFACE);
	if(g_pScreen == NULL)
	{
		fprintf(g_pLogFile, "ERROR - SDL_SetVideoMode(): %s\n", SDL_GetError());
		return false;
	}

	// Even though the Joystick subsystem was initialized
	// and even though we'll never use this object, we still have to open it
	// so that the event subsystem will capture the joystick events.
	g_pJoystick = SDL_JoystickOpen(0);
	if(g_pJoystick == NULL)
	{
		fprintf(g_pLogFile, "ERROR - SDL_JoystickOpen(): %s\n", SDL_GetError());
		return false;
	}

	//SDL_ShowCursor(SDL_DISABLE);

	return true;
}

////////////////////////////////////////////////////////////////////////////////

void Draw(Uint16 iColor)
{
	SDL_LockSurface(g_pScreen);

	Uint16* pPixels = (Uint16*)g_pScreen->pixels;

	pPixels[ ( g_iY * g_pScreen->w ) + g_iX ] = iColor;

	SDL_UnlockSurface(g_pScreen);

	g_bNeedUpdate = true;
}

void Clear()
{
	SDL_Rect	rec;

	rec.x = 0;
	rec.y = 0;
	rec.w = MAX_X;
	rec.h = MAX_Y;

	SDL_LockSurface(g_pScreen);

	SDL_FillRect(g_pScreen, &rec, BLACK);

	SDL_UnlockSurface(g_pScreen);

	g_bNeedUpdate = true;
}

void DoUp()		{ if(g_iY > MIN_Y)		{ g_iY--; g_bNeedUpdate = true; } }
void DoDown()		{ if(g_iY < MAX_Y-1)	{ g_iY++; g_bNeedUpdate = true; } }
void DoLeft()		{ if(g_iX > MIN_X)		{ g_iX--; g_bNeedUpdate = true; } }
void DoRight()		{ if(g_iX < MAX_X-1)	{ g_iX++; g_bNeedUpdate = true; } }

void Run()
{
	fprintf(g_pLogFile, "Run()\n");

	SDL_Event		event;

	while(1)
	{
		SDL_PollEvent(&event);

		if(g_bNeedUpdate)
		{
			UpdateScreen();
			g_bNeedUpdate = false;
		}

		switch(event.type)
		{
		case SDL_JOYBUTTONDOWN:
			//fprintf(g_pLogFile, "Button Down: %d\n", event.jbutton.button);

			g_input[event.jbutton.button] = true;
			break;

		case SDL_JOYBUTTONUP:
			//fprintf(g_pLogFile, "Button Down: %d\n", event.jbutton.button);

			g_input[event.jbutton.button] = false;
			break;

		default:
			break;
		}

		for(int i = 0; i < GP2X_INPUT_MAX; i++)
		{
			if(g_input[i] == false)
				continue;

			switch(i)
			{
			case GP2X_JOY_UP:			DoUp();					break;
			case GP2X_JOY_UP_LEFT:		DoUp();		DoLeft();		break;
			case GP2X_JOY_LEFT:						DoLeft();		break;
			case GP2X_JOY_DOWN_LEFT:		DoDown();		DoLeft();		break;
			case GP2X_JOY_DOWN:			DoDown();					break;
			case GP2X_JOY_DOWN_RIGHT:	DoDown();		DoRight();	break;
			case GP2X_JOY_RIGHT:					DoRight();	break;
			case GP2X_JOY_UP_RIGHT:		DoUp();		DoRight();	break;
			case GP2X_START:			return;
			case GP2X_SELECT:			Clear();					break;
			case GP2X_R:				Draw(PINK);				break;
			case GP2X_L:				Draw(YELLOW);				break;
			case GP2X_A:				Draw(RED);				break;
			case GP2X_B:				Draw(BLUE);				break;
			case GP2X_Y:				Draw(GREEN);				break;
			case GP2X_X:				Draw(WHITE);				break;
			case GP2X_VOL_UP:			break;
			case GP2X_VOL_DOWN:			break;
			case GP2X_JOY_PUSH:			break;
			}
		}
	}



}

////////////////////////////////////////////////////////////////////////////////

void ShutDown()
{
	if(g_pLogFile)
	{
		fprintf(g_pLogFile, "ShutDown()\n");
		fclose(g_pLogFile);
	}

	SDL_Quit();

	// Return control to the menu program.
	chdir("/usr/gp2x");
	execl("/usr/gp2x/gp2xmenu", "/usr/gp2x/gp2xmenu", NULL);
}

////////////////////////////////////////////////////////////////////////////////
// A really bad way to do this but its good enough for a test program.
// A better way would be to only redraw/reblit the dirty sections.

void UpdateScreen()
{
	SDL_WarpMouse(g_iX, g_iY);
	SDL_Flip(g_pScreen);
}

////////////////////////////////////////////////////////////////////////////////
